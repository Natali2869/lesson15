import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public class TestHelper {

    private TestHelper() {}

    public static void main(String[] args) throws Exception {


        System.out.println(getRandomYear());
        System.out.println(getRandomLong());
        System.out.println(getRandomWords());
        System.out.println(readProps());
        System.out.println(reformatDate("05.02.2022"));
        System.out.println(toDouble("05.02"));




    }

    public static int getRandomYear() {
        Calendar now = Calendar.getInstance();
        int currentYear = now.get(Calendar.YEAR);

        return new Random().nextInt(currentYear + 1 - 1970) + 1970;
    }

    public static long getRandomLong() {
        return new Random().nextLong();
    }

    public static String getRandomWords() {
        String line = "";
        String abc = "abcdefghijklmnopqrstuvwxyz";

        for (int i = 0; i < 3; i++) {
            int wordLength = new Random().nextInt(4) + 5;
            String word = "";
            for (int j = 0; j < wordLength; j++) {
                word += abc.charAt(new Random().nextInt(abc.length()));
            }
            word = word.substring(0, 1).toUpperCase() + word.substring(1);
            line += " " + word;
        }

        return line.trim();
    }

    public static HashMap<String, String> readProps() {
        HashMap<String, String> map = new HashMap<>();

        try {
            BufferedReader r = new BufferedReader(new FileReader("src/main/java/props.txt"));
            String line;
            while ((line = r.readLine()) != null) {
                if (line.matches(".+::.+")) {
                    String[] parts = line.split("::");
                    map.put(parts[0].trim(), parts[1].trim());
                }
            }
            r.close();
        } catch (Exception e) {

        }

        return map;
    }

    public static String reformatDate(String dateStr) throws ParseException {
        DateFormat dfEU = new SimpleDateFormat("dd.MM.yyyy");
        DateFormat dfUS = new SimpleDateFormat("MM/dd/yyyy");

        Date date = dfEU.parse(dateStr);

        return dfUS.format(date);
    }

    public static Double toDouble(String str) {
        double number;

        try {
            number = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            number = Double.POSITIVE_INFINITY;
        }

        return number;
    }
}

