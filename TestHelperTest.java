import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import org.junit.Assert;
import org.junit.Test;

public class TestHelperTest {

    @Test
    public void getRandomYearTest() {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        for (int i = 0; i < 100; i++) {
            int randomYear = TestHelper.getRandomYear();
            Assert.assertTrue(randomYear >= 1970);
            Assert.assertTrue(randomYear <= currentYear);
        }
    }

    @Test
    public void getRandomLongTest() {
        boolean different = false;

        long prev = TestHelper.getRandomLong();
        for (int i = 0; i < 100; i++) {
            long current = TestHelper.getRandomLong();
            different |= prev != current;
            prev = current;
        }

        Assert.assertTrue(different);
    }

    @Test
    public void getRandomWordsTest() {
        String line = TestHelper.getRandomWords();

        Assert.assertNotNull(line);

        String[] words = line.split(" ");

        Assert.assertEquals(words.length, 3);

        for (String word : words) {
            Assert.assertTrue(word.length() > 1);
            String firstLetter = word.substring(0, 1);
            String wordReminder = word.substring(1);
            Assert.assertEquals(firstLetter.toLowerCase() + wordReminder, word.toLowerCase());
            Assert.assertEquals(firstLetter + wordReminder.toUpperCase(), word.toUpperCase());
        }
    }

    @Test
    public void readPropsTest() {
        HashMap<String, String> props = TestHelper.readProps();

        Assert.assertNotNull(props);
        Assert.assertTrue(props.size() > 0);

        for (String key : props.keySet()) {
            Assert.assertNotNull(key);
            Assert.assertNotNull(props.get(key));
        }
    }

    @Test
    public void reformatDateTest() {
        String dateEU = "30.01.2022";
        String dateUS = "01/30/2022";

        try {
            String dateToTest = TestHelper.reformatDate(dateEU);
            Assert.assertEquals(dateToTest, dateUS);
        } catch (ParseException pe) {
            Assert.fail();
        }
    }

    @Test
    public void toDouble() {
        String validDouble = "123.456";
        double validDoubleNum = 123.456;
        String invalidDouble = "Blablabla";

        Double fromValid = TestHelper.toDouble(validDouble);
        Assert.assertNotNull(fromValid);
        Assert.assertFalse(fromValid.isNaN());
        Assert.assertFalse(fromValid.isInfinite());
        Assert.assertTrue(Math.abs(fromValid - validDoubleNum) < 0.0001);

        Double fromInvalid = TestHelper.toDouble(invalidDouble);
        Assert.assertNotNull(fromInvalid);
        Assert.assertTrue(fromInvalid.isInfinite());
    }

}
